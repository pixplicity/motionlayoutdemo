package com.pixplicity.animationdemo

import android.content.Context
import android.content.res.Resources
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import kotlinx.android.synthetic.main.li_item.view.*


class CounterAdapter(val context: Context) : RecyclerView.Adapter<CounterAdapter.ViewHolder>() {

//    val width: Int = Resources.getSystem().displayMetrics.widthPixels
//    val height: Int = Resources.getSystem().displayMetrics.heightPixels
    var entranceState: Int? = null

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val avatar = itemView.iv_added_item
        val container = itemView.added_item_container
    }

    val items = arrayListOf<Int>()

    fun addItem() {
        val count = items.size+1
        items.add(count)
        notifyItemInserted(count)
        notifyItemRangeChanged(items.size-1, items.size)
    }

    fun removeItem() {
        val count = items.size-1
        if (count > 0) {
            items.remove(items.size)
            notifyItemRemoved(items.size)
            notifyItemRangeChanged(items.size - 1, items.size)
        }
    }

    var lastPosition = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CounterAdapter.ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.li_item, parent, false))
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: CounterAdapter.ViewHolder, position: Int) {
        // initialize entrance state
        if (entranceState == null) {
            entranceState = holder.container.currentState
        }
        holder.avatar.setImageDrawable(context.getDrawable(R.drawable.ic_pix_wireframe))
        if (holder.adapterPosition > lastPosition || lastPosition == 0 && holder.adapterPosition == 0) {
            Log.d("CounterAdapter", "Animate ${holder.adapterPosition} - $position - $lastPosition")
            Log.d("CounterAdapter", "entrance state: ${holder.container.currentState}")
            val slideInAnimation = AnimationUtils.loadAnimation(context, R.anim.anim_slide_in_from_left)
            holder.itemView.animation = slideInAnimation
            holder.container.transitionToEnd()
            Log.d("CounterAdapter", "final state: ${holder.container.currentState}")
            holder.container.transitionToStart()
            // reset the state once the animation is done
//            holder.container.transitionToState(entranceState!!)
        } else {
            Log.d("CounterAdapter", "Didn't animate ${holder.adapterPosition} - $position - $lastPosition")
        }
        lastPosition = holder.adapterPosition
    }
}

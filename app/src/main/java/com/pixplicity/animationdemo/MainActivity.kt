package com.pixplicity.animationdemo

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.app.AppCompatDelegate
import kotlinx.android.synthetic.main.activity_main_start.*

class MainActivity : AppCompatActivity() {
    private var counter = 0
    private lateinit var list: List<Int>
    private lateinit var counterAdapter: CounterAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_start)
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)

        tv_counter.text = counter.toString()

        list = arrayListOf()
        counterAdapter = CounterAdapter(this@MainActivity)
        rv_added_items.apply {
            setHasFixedSize(false)
            adapter = counterAdapter
        }

        bt_increase.setOnClickListener {
            counterAdapter.addItem()
            tv_counter.text = "${counterAdapter.itemCount}"
        }
        bt_decrease.setOnClickListener {
            counterAdapter.removeItem()
            tv_counter.text = "${counterAdapter.itemCount}"
        }
    }
}